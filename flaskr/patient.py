from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('patient', __name__)

@bp.route('/view_transaction', methods=('GET', 'POST'))
@login_required
def view_transaction():

    return render_template('patient/viewTransaction.html')

@bp.route('/checkup', methods=('GET', 'POST'))
@login_required
def checkup():
    db = get_db()
    histories = db.execute('SELECT c.date, p.policlinic_name, s.time_interval, d.doctor_name, m.symptoms, m.diagnosys, t.treatment_name FROM checkup_schedule c\
                            JOIN medical_record m ON c.checkup_schedule_id = m.checkup_schedule_id\
                            JOIN doctor_shift ds on c.doctor_shift_id = ds.doctor_shift_id\
                            JOIN doctor d on ds.doctor_id = d.doctor_id\
                            JOIN shift s on ds.shift_id = s.shift_id\
                            JOIN policlinic p ON d.policlinic_id = p.policlinic_id\
                            JOIN checkup_treatment ct ON m.checkup_treatment_id = ct.checkup_treatment_id\
                            JOIN treatment t ON ct.treatment_id = t.treatment_id\
                            WHERE c.patient_id = ?',
                         (session['user_id'],)).fetchall()

    lst = []
    for history in histories:
        interval = history['time_interval']
        interval = interval[12:17] + " - " + interval[30:35]
        history = tuple(interval if history['time_interval'] == x else x for x in history)
        lst.append(history)

    if request.method == 'POST':
        policlinicType = request.form.get('selectType')
        date = request.form['date']

        db = get_db()
        return redirect(url_for('patient.schedule', id=policlinicType, date=date))

    print(histories)    
    return render_template('patient/history.html', histories=lst)

@bp.route('/schedule/<id>/<date>', methods=('GET', 'POST'))
@login_required
def schedule(id, date):
    db = get_db()
    schedules = db.execute('SELECT s.time_interval, d.doctor_name, sp.specialization_name, r.room_name, ds.doctor_shift_id, c.patient_id\
                FROM shift s JOIN doctor_shift ds ON s.shift_id = ds.shift_id\
                JOIN room r on ds.room_id = r.room_id\
                JOIN doctor d on ds.doctor_id = d.doctor_id\
                JOIN specialization sp on d.specialization_id = sp.specialization_id\
                LEFT OUTER JOIN checkup_schedule c ON ds.doctor_shift_id = c.doctor_shift_id\
                WHERE ds.policlinic_id = ? and (c.patient_id = ? or c.patient_id is null)\
                GROUP BY ds.doctor_shift_id, s.time_interval, d.doctor_name, sp.specialization_name, r.room_name, d.doctor_id, c.patient_id', (id, session['user_id'])).fetchall()

    booked = db.execute('SELECT patient_id FROM checkup_schedule WHERE patient_id = ?',
                        (session['user_id'],)).fetchall()

    for i in range(0,len(booked)):
            booked[i] = booked[i][0]

    lst = []
    for schedule in schedules:
        interval = schedule['time_interval']
        interval = interval[12:17] + " - " + interval[30:35]
        schedule = tuple(interval if schedule['time_interval'] == x else x for x in schedule)
        print(schedule[5])
        if schedule[5] in booked:
            schedule = schedule + tuple('1')
        else:
            schedule = schedule + tuple('0')
        print(schedule[6])
        lst.append(schedule)

    if request.method == 'POST':        
        doc_id = request.form['doc']
        db.execute('INSERT into checkup_schedule (doctor_shift_id, patient_id, date) VALUES (?, ?, ?)',
                   (doc_id, session['user_id'], str.encode(date)))
        db.commit()
        
        return redirect(url_for('patient.schedule', id=id, date=date))

    test = db.execute('SELECT * FROM checkup_schedule').fetchall()
    for a in booked:
        print("booked:",a)
    return render_template('patient/schedule.html', schedules=lst)
