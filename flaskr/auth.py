import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register/patient', methods=('GET', 'POST'))
def register_patient():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        phone = request.form['phone']
        dob = request.form['dob']
        allergies = request.form['allergies']
        userType = request.form.get('selectType')
        user_id = max([request.form['cardId'], request.form['ssoId']], key=len)
        
        db = get_db()
        error = check_form(username, password, email, phone)

        if db.execute(
            'SELECT sso_id FROM civitas_patient WHERE sso_id = ?', (user_id,)
        ).fetchone() is not None:
            error = 'SSO ID {} is already registered.'.format(user_id)
        elif db.execute(
            'SELECT id_card_no FROM public_patient WHERE id_card_no = ?', (user_id,)
        ).fetchone() is not None:
            error = 'ID Card {} is already registered.'.format(user_id)
        
        if error is None:
            db.execute(
                'INSERT INTO users (username, password, email, phone) VALUES (?, ?, ?, ?)',
                (username, generate_password_hash(password), email, phone))
            db.execute(
                'INSERT INTO patient (dob, allergies, username) VALUES (?, ?, ?)',
                (dob, allergies, username))
            patient = db.execute(
                'SELECT patient_id FROM patient WHERE username = ?', (username,)
                ).fetchone()
            
            if userType == 'public':
                db.execute(
                    'INSERT INTO public_patient (id_card_no, patient_id) VALUES (?, ?)',
                    (user_id, patient['patient_id']))
            elif userType == 'civitas':
                db.execute(
                    'INSERT INTO civitas_patient (sso_id, patient_id) VALUES (?, ?)',
                    (user_id, patient['patient_id']))
                
            db.commit()
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register-patient.html')

@bp.route('/register/doctor', methods=('GET', 'POST'))
def register_doctor():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        phone = request.form['phone']
        name = request.form['doctorName']
        license_no = request.form['licenseNo']
        specialization = request.form['specialization']
        policlinic_id = request.form.get('type')

        db = get_db()
        error = check_form(username, password, email, phone)

        if error is None:
            db.execute(
                'INSERT INTO users (username, password, email, phone) VALUES (?, ?, ?, ?)',
                (username, generate_password_hash(password), email, phone))
            db.execute(
                'INSERT INTO specialization (specialization_name, policlinic_id) VALUES (?, ?)',
                (specialization, policlinic_id))
            specialization_id = get_specialization_id(specialization)
            db.execute(
                'INSERT INTO doctor (doctor_name, license_no, specialization_id, policlinic_id, username) VALUES (?, ?, ?, ?, ?)',
                (name, license_no, specialization_id, policlinic_id, username))

            db.commit()
            return redirect(url_for('auth.login'))

        flash(error)
        
    return render_template('auth/register-doctor.html')

@bp.route('/register/pharmacist', methods=('GET', 'POST'))
def register_pharmacist():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        phone = request.form['phone']
        license_no = request.form['licenseNo']

        db = get_db()
        error = check_form(username, password, email, phone)

        if error is None:
            db.execute(
                'INSERT INTO users (username, password, email, phone) VALUES (?, ?, ?, ?)',
                (username, generate_password_hash(password), email, phone))
            db.execute(
                'INSERT INTO pharmacist (license_no, username) VALUES (?, ?)',
                (license_no, username))

            db.commit()
            return redirect(url_for('auth.login'))

        flash(error)
        
    return render_template('auth/register-pharmacist.html')

@bp.route('/register/cashier', methods=('GET', 'POST'))
def register_cashier():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        phone = request.form['phone']

        db = get_db()
        error = check_form(username, password, email, phone)

        if error is None:
            db.execute(
                'INSERT INTO users (username, password, email, phone) VALUES (?, ?, ?, ?)',
                (username, generate_password_hash(password), email, phone))
            db.execute(
                'INSERT INTO cashier (username) VALUES (?)',
                (username,))

            db.commit()
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register-cashier.html')
                
@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
            'SELECT * FROM users WHERE username = ?', (username,)
        ).fetchone()

        if user is None:
            error = 'Incorrect username.'
        elif not check_password_hash(user['password'], password):
            error = 'Incorrect password.'

        if error is None:
            session.clear()
            session['username'] = user['username']
            session['user_type'] = get_user_type(user['username'])
            session['user_id'] = get_user_id(session['user_type'], session['username'])
            return redirect(url_for('index'))

        flash(error)

    return render_template('auth/login.html')

@bp.before_app_request
def load_logged_in_user():
    username = session.get('username')
    db = get_db()

    if username is None:
        g.user = None
    else:
        g.user_id = session.get('user_id')
        g.user = db.execute(
            'SELECT * FROM users WHERE username = ?', (username,)
        ).fetchone()
        g.type = session.get('user_type')
        g.phone = db.execute(
            'SELECT phone FROM users WHERE username = ?', (username,)
        ).fetchone()[0]
        
        if g.type == 'public_patient' or g.type == 'civitas_patient':
            g.dob = db.execute(
                'SELECT dob FROM patient WHERE username = ?', (username,)
                ).fetchone()[0]
            g.allergies = db.execute(
                'SELECT allergies FROM patient WHERE username = ?', (username,)
                ).fetchone()[0]
            if g.type == 'public_patient':
                g.id = db.execute(
                    'SELECT id_card_no FROM public_patient WHERE patient_id IN (SELECT patient_id FROM patient WHERE username = ?)'
                    ,(username,)).fetchone()[0]
            elif g.type == 'civitas_patient':
                g.id = db.execute(
                    'SELECT sso_id FROM civitas_patient WHERE patient_id IN (SELECT patient_id FROM patient WHERE username = ?)'
                    ,(username,)).fetchone()[0]
                
        elif g.type == 'doctor' or g.type == 'pharmacist':
            if g.type == 'doctor':
                g.name = db.execute(
                    'SELECT doctor_name FROM doctor WHERE username = ?', (username,)
                    ).fetchone()[0]
                g.special = db.execute(
                    'SELECT specialization_id FROM doctor WHERE username= ?', (username,)
                    ).fetchone()[0]

                g.pol_id = db.execute(
                    'SELECT policlinic_id FROM doctor WHERE username= ? ', (username,)
                    ).fetchone()[0]

            if g.type == 'doctor' or g.type == 'pharmacist':
                if g.type == 'doctor':
                    g.license = db.execute(
                        'SELECT license_no FROM doctor WHERE username = ?', (username,)
                        ).fetchone()[0]
                else:
                    g.license = db.execute(
                        'SELECT license_no FROM pharmacist WHERE username = ?', (username,)
                        ).fetchone()[0]

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view

def check_form(username, password, email, phone):
    db = get_db()
    if not username:
        return 'Username is required.'
    elif not password:
        return 'Password is required.'
    elif not phone.isdigit():
        return 'Wrong format for phone number.'
    elif db.execute(
        'SELECT username FROM users WHERE username = ?', (username,)
    ).fetchone() is not None:
        return 'User {} is already registered.'.format(username)
    elif db.execute(
        'SELECT username FROM users WHERE email = ?', (email,)
    ).fetchone() is not None:
        return 'Email {} is already registered.'.format(email)
    else:
        return None

def get_user_type(username):
    db = get_db()
    if db.execute(
        'SELECT * FROM public_patient WHERE patient_id in (SELECT patient_id FROM patient WHERE username = ?)',
        (username,)
    ).fetchone() is not None:
        return "public_patient"

    elif db.execute(
        'SELECT * FROM civitas_patient WHERE patient_id in (SELECT patient_id FROM patient WHERE username = ?)',
        (username,)
    ).fetchone() is not None:
        return "civitas_patient"

    elif db.execute(
        'SELECT * FROM doctor WHERE username = ?',
        (username,)
    ).fetchone() is not None:
        return "doctor"

    elif db.execute(
        'SELECT * FROM pharmacist WHERE username = ?',
        (username,)
    ).fetchone() is not None:
        return "pharmacist"

    elif db.execute(
        'SELECT * FROM cashier WHERE username = ?',
        (username,)
    ).fetchone() is not None:
        return "cashier"

def get_user_id(user_type, username):
    db = get_db()
    if user_type == 'public_patient' or user_type == 'civitas_patient':
        return db.execute(
            'SELECT patient_id FROM patient where username = ?',
            (username,)
        ).fetchone()[0]
    elif user_type == 'doctor':
        return db.execute(
            'SELECT doctor_id FROM doctor where username = ?',
            (username,)
        ).fetchone()[0]
    elif user_type == 'pharmacist':
        db.execute(
            'SELECT pharmacist_id FROM pharmacist where username = ?',
            (username,)
        ).fetchone()[0]
    elif user_type == 'cashier':
        db.execute(
            'SELECT cashier_id FROM cashier where username = ?',
            (username,)
        ).fetchone()[0]
        
def get_specialization_id(specialization):
    db = get_db()
    return db.execute('SELECT specialization_id FROM specialization WHERE specialization_name = ?',
                         (specialization,)).fetchone()[0]
