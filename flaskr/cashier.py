from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('cashier', __name__)

@bp.route('/update_transaction', methods=('GET', 'POST'))
@login_required
def update_transaction():

    return render_template('cashier/updateTransaction.html')
