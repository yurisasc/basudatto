from flask import (
    Blueprint, flash, g, redirect, render_template, session, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('doctor', __name__)

@bp.route('/schedule', methods=('GET', 'POST'))
@login_required
def get_schedule():
    db = get_db()
    schedules = db.execute(
		'SELECT cs.checkup_schedule_id, cs.date, s.shift_type, r.room_name '
		'FROM checkup_schedule cs, doctor_shift ds, shift s, room r '
		'WHERE cs.doctor_shift_id=ds.doctor_shift_id AND ds.shift_id=s.shift_id AND ds.room_id=r.room_id '
        'AND ds.doctor_id=?',
        (session['user_id'],)
    ).fetchall()
    return render_template('doctor/schedule.html', schedules=schedules)

@bp.route('/patient-list/<checkup_schedule_id>&<date>&<shift>&<room>', methods=('GET', 'POST'))
@login_required
def patient_list(checkup_schedule_id, date, shift, room):
	db = get_db()
	lists = db.execute(
		'SELECT cs.patient_id FROM checkup_schedule cs JOIN doctor_shift ds ON cs.doctor_shift_id=ds.doctor_shift_id',
		).fetchall()

	shifts1 = db.execute('SELECT * from doctor_shift').fetchall()

	checkups = db.execute('SELECT * from checkup_schedule').fetchall()

	for shift1 in shifts1:
            print("SHIFT")
            print("doctor_shift_id",shift1[0])
            print("doctor_id",shift1[1])
            print("shift_id",shift1[2])
            print("policlinic_id",shift1[3])
            print("room_id",shift1[4])
	for checkup in checkups:
            print("CHECKUP")
            print("checkup_schedule_id",checkup[0])
            print("doctor_shift_id",checkup[1])
            print("patient_id",checkup[2])
            print("date",checkup[3])
	return render_template('doctor/patient-list.html', checkup_id=checkup_schedule_id, date=date, shift=shift, room=room, lists=lists)

@bp.route('/add-medical-record/<date>&<shift>&<room>&<checkup_id>&<patient_id>', methods=('GET', 'POST'))
@login_required
def add_medical_record(date, shift, room, checkup_id, patient_id):
    db = get_db()
    treatments = db.execute('SELECT treatment_id, treatment_name FROM treatment')

    if request.method == 'POST':
        symptoms = request.form['symptoms']
        diagnosys = request.form['diagnosys']
        lst = []
        counter = 0;
        while True:
            strs = "select-treatment"+str(counter)
            e = request.form.get(strs)
            if e is None:
                break
            lst.append(e)
            counter += 1
            print(e)
                
        
        print("Hello",e)
        error = None

        if not symptoms:
            error = 'Please write down the symptoms!'

        elif not diagnosys:
            error = 'Please write down the diagnosys!'

        if error is not None:
            flash(error)
        else:
            db.execute('INSERT INTO id_tracker (dummy) VALUES ("dummy")')
            db.commit()
            unique_id = db.execute('SELECT tracker_id FROM id_tracker ORDER BY tracker_id DESC').fetchall()[0][0];

            for treatment_id in lst:
                db.execute('INSERT INTO checkup_treatment (checkup_treatment_id, checkup_schedule_id, treatment_id) VALUES (?, ?, ?)',
                            (unique_id,checkup_id,treatment_id))
            db.commit()

            db.execute(
                'INSERT INTO medical_record (checkup_schedule_id, symptoms, diagnosys, '
                'checkup_treatment_id, patient_id) VALUES (?, ?, ?, ?, ?)',
                (checkup_id, symptoms, diagnosys, unique_id, patient_id)
            )
            db.commit()
            return redirect(url_for('doctor.get_schedule'))

    return render_template('doctor/add-medical-record.html', date=date, shift=shift, room=room, patient_id=patient_id, treatments=treatments)

@bp.route('/shift', methods=('GET', 'POST'))
@login_required
def shift():
    if session['user_type'] == 'doctor':
        doctor_id = session['user_id']
        
        db = get_db()
        shifts = db.execute(
            'SELECT s.shift_id, s.day, s.shift_type, s.time_interval\
            FROM shift s ORDER BY s.shift_id').fetchall()

        applied = db.execute(
            'SELECT shift_id FROM doctor_shift WHERE doctor_id = ?',
            (doctor_id,)).fetchall()

        for i in range(0,len(applied)):
            applied[i] = applied[i][0]
        
        lst = []
        for shift in shifts:
            interval = shift['time_interval']
            interval = interval[12:17] + " - " + interval[30:35]
            shift = tuple(interval if shift['time_interval'] == x else x for x in shift)

            if shift[0] in applied:
                shift = shift + tuple('1')
            else:
                shift = shift + tuple('0')
            lst.append(shift)

        if request.method == 'POST':
            shift_id = request.form['shift_id']

            exist = db.execute('SELECT * FROM shift s LEFT OUTER JOIN doctor_shift ds\
                                ON s.shift_id = ds.shift_id\
                                WHERE s.shift_id = ? and ds.doctor_id = ?',
                       (shift_id, doctor_id)).fetchone()

            if exist is None:
                rule1 = db.execute('SELECT r.room_id FROM room r JOIN doctor_shift ds\
                                    ON r.room_id = ds.room_id\
                                    WHERE ds.doctor_id != ? and ds.shift_id = ?',
                                   (doctor_id, shift_id)).fetchall()
                
                rule2 = db.execute('SELECT r.room_id FROM room r JOIN doctor_shift ds\
                                    ON r.room_id = ds.room_id\
                                    WHERE ds.doctor_id = ? and ds.shift_id = ?',
                                   (doctor_id, shift_id)).fetchall()
                rule = list(set().union(rule1,rule2))
                
                rooms = db.execute('SELECT room_id FROM room').fetchall()

                available = None
                for room in rooms:
                    if room['room_id'] not in rule:
                        available = room['room_id']
                        break
                        
                db.execute(
                    'INSERT INTO doctor_shift (doctor_id, shift_id, policlinic_id, room_id) VALUES (?, ?, ?, ?)',
                    (session['user_id'], shift_id, g.pol_id, str(available)))
                db.commit()

                print('RULE1', rule)
                print('RULE2:',rule2)
            else:
                db.execute('DELETE FROM doctor_shift WHERE doctor_id = ? and shift_id = ?',
                           (doctor_id, shift_id))
                db.commit()

            return redirect(url_for('doctor.shift'))
        
        return render_template('doctor/shift.html', shifts=lst)
    
    else:
        return redirect(url_for('index'))
