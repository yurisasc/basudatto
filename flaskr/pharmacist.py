from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('pharmacist', __name__)

@bp.route('/medicine', methods=('GET', 'POST'))
@login_required
def medicine():
        
     db = get_db()
     medicines = db.execute(
          'SELECT m.medicine_name, m.price, m.stock '
	  'FROM medicine m '
     ).fetchall()
    
     return render_template('pharmacist/medicine.html', medicines = medicines)

def add_medicine():

    if request.method == 'POST':
            db = get_db()
            db.execute(
                'INSERT INTO medicine (medicine_name, price, stock)'
                'VALUES (medicine_name, price, stock)',
                (pharmacy_id, pharmacist_id)
            )
            db.commit()
            return redirect(url_for('pharmacist.get_medicine'))

    return render_template('medicine/medicine?')

#test
