DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS patient;
DROP TABLE IF EXISTS civitas_patient;
DROP TABLE IF EXISTS public_patient;
DROP TABLE IF EXISTS service;
DROP TABLE IF EXISTS policlinic;
DROP TABLE IF EXISTS specialization;
DROP TABLE IF EXISTS doctor;
DROP TABLE IF EXISTS treatment;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS shift;
DROP TABLE IF EXISTS doctor_shift;
DROP TABLE IF EXISTS checkup_schedule;
DROP TABLE IF EXISTS checkup_treatment;
DROP TABLE IF EXISTS medical_record;
DROP TABLE IF EXISTS cashier;
DROP TABLE IF EXISTS checkup_payment;
DROP TABLE IF EXISTS pharmacist;
DROP TABLE IF EXISTS pharmacy;
DROP TABLE IF EXISTS medicine;
DROP TABLE IF EXISTS prescription_order;
DROP TABLE IF EXISTS prescription_payment;
DROP TABLE IF EXISTS transaction_payment;
DROP TABLE IF EXISTS id_tracker;

CREATE TABLE users(
username varchar(20) not null,
password varchar(20) not null,
email varchar(20) not null,
phone varchar(12) not null,
primary key (username)
);

create table patient(
patient_id INTEGER PRIMARY KEY,
dob date not null,
allergies varchar(50),
username varchar(20) not null,
foreign key (username) references users(username)
on update cascade on delete cascade
);

create table civitas_patient(
sso_id varchar(20) not null,
patient_id int not null,
primary key (sso_id),
foreign key (patient_id) references patient(patient_id)
on update cascade on delete cascade
);

create table public_patient(
id_card_no varchar(20) not null,
patient_id int not null,
primary key (id_card_no),
foreign key (patient_id) references patient(patient_id)
on update cascade on delete cascade
);

create table service(
id_service int not null,
service_name varchar(20) check (service_name='Policlinic' or service_name='Pharmacy'),
primary key (id_service)
);

create table policlinic(
policlinic_id int not null,
policlinic_name varchar(20) check (policlinic_name='General' or policlinic_name='Dentist' or policlinic_name='Counseling' or policlinic_name='Radiologist'),
service_id int not null,
primary key (policlinic_id),
foreign key (service_id) references service(id_service)
on update cascade on delete cascade
);

create table specialization(
specialization_id INTEGER PRIMARY KEY,
specialization_name varchar(20) not null,
policlinic_id int not null,
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade
);

create table doctor(
doctor_id INTEGER PRIMARY KEY,
doctor_name varchar(50) not null,
license_no varchar(20) not null,
specialization_id int not null,
policlinic_id int not null,
username varchar(20) not null,
foreign key (username) references users(username),
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade,
foreign key (specialization_id) references specialization(specialization_id)
on update cascade on delete cascade
);

create table treatment(
treatment_id int not null,
treatment_name varchar(20) not null,
price float not null,
policlinic_id int not null,
primary key (treatment_id),
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade
);

create table room(
room_id INTEGER PRIMARY KEY,
room_name varchar(20) not null,
policlinic_id int not null,
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade
);

create table shift(
shift_id INTEGER PRIMARY KEY,
day varchar(10) not null check (day = 'Sunday' or day = 'Monday' or day = 'Tuesday' or day = 'Wednesday' or day = 'Thursday' or day = 'Friday' or day = 'Saturday'),
shift_type varchar(10) not null check (shift_type = 'Morning' or shift_type = 'Afternoon' or shift_type = 'Evening'),
time_interval tsrange not null
);

create table doctor_shift(
doctor_shift_id INTEGER PRIMARY KEY,
doctor_id int not null,
shift_id int not null,
policlinic_id int not null,
room_id int not null,
foreign key (doctor_id) references doctor(doctor_id)
on update cascade on delete cascade,
foreign key (shift_id) references shift(shift_id)
on update cascade on delete cascade,
foreign key (policlinic_id) references policlinic(policlinic_id)
on update cascade on delete cascade,
foreign key (room_id) references room(room_id)
on update cascade on delete cascade
);

create table checkup_schedule(
checkup_schedule_id INTEGER PRIMARY KEY,
doctor_shift_id int not null,
patient_id int not null,
date date not null,
foreign key (doctor_shift_id) references doctor_shift(doctor_shift_id)
on update cascade on delete cascade,
foreign key (patient_id) references patient(patient_id)
on update cascade on delete cascade
);

create table checkup_treatment(
checkup_treatment_id int not null,
checkup_schedule_id int not null,
treatment_id int not null,
unique(checkup_treatment_id),
foreign key (checkup_schedule_id) references checkup_schedule(checkup_schedule_id)
on update cascade on delete cascade,
foreign key (treatment_id) references treatment(treatment_id)
on update cascade on delete cascade
);

create table medical_record(
medical_record_id INTEGER PRIMARY KEY,
checkup_schedule_id int not null,
symptoms varchar(50),
diagnosys varchar(50),
checkup_treatment_id int not null,
patient_id int not null,
foreign key (checkup_schedule_id) references checkup_schedule(checkup_schedule_id)
on update cascade on delete cascade,
foreign key (checkup_treatment_id) references checkup_treatment(checkup_treatment_id)
on update cascade on delete cascade,
foreign key (patient_id) references patient(patient_id)
on update cascade on delete cascade
);

CREATE TABLE CASHIER(
cashier_id INTEGER PRIMARY KEY,
username VARCHAR(20) NOT NULL,
FOREIGN KEY (username) REFERENCES USERS(username)
ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE CHECKUP_PAYMENT(
checkup_payment_id INT NOT NULL,
checkup_schedule_id INT NOT NULL,
id_public_card VARCHAR(20),
total_payment FLOAT NOT NULL,
status VARCHAR(10) NOT NULL,
recorded_by INT NOT NULL,
PRIMARY KEY (checkup_payment_id),
FOREIGN KEY (checkup_schedule_id) REFERENCES CHECKUP_SCHEDULE(checkup_schedule_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (id_public_card) REFERENCES PUBLIC_PATIENT(id_card_no)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (recorded_by) REFERENCES CASHIER(cashier_id)

ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE PHARMACIST(
pharmacist_id INTEGER PRIMARY KEY,
license_no VARCHAR(20) NOT NULL,
username VARCHAR(20) NOT NULL,
FOREIGN KEY (username) REFERENCES USERS(username)
ON UPDATE CASCADE ON DELETE CASCADE
);

create table pharmacy(
pharmacy_id int not null,
license_no int not null,
primary key (pharmacy_id)
);

CREATE TABLE MEDICINE(
medicine_id INT NOT NULL,
medicine_name VARCHAR(50) NOT NULL,
price FLOAT NOT NULL,
stock INT NOT NULL,
pharmacy_id INT NOT NULL,
pharmacist_id INT NOT NULL,
PRIMARY KEY (medicine_id),
FOREIGN KEY (pharmacy_id) REFERENCES PHARMACY(pharmacy_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (pharmacist_id) REFERENCES PHARMACIST(pharmacist_id)
ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE PRESCRIPTION_ORDER(
prescription_id INT NOT NULL,
patient_id INT NOT NULL,
medicine_id INT NOT NULL,
order_date DATE NOT NULL,
PRIMARY KEY (prescription_id),
FOREIGN KEY (patient_id) REFERENCES PATIENT(patient_id)

ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (medicine_id) REFERENCES MEDICINE(medicine_id)
ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE PRESCRIPTION_PAYMENT(
prescription_payment_id INT NOT NULL,
prescription_id INT NOT NULL,
public_card_no varchar(20) NOT NULL,
total_payment FLOAT NOT NULL,
status varchar(10) NOT NULL,
PRIMARY KEY(prescription_payment_id),
FOREIGN KEY (prescription_id) REFERENCES PRESCRIPTION_ORDER(prescription_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (public_card_no) REFERENCES PUBLIC_PATIENT(id_card_no)
ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE TRANSACTION_PAYMENT(
transaction_payment_id INT NOT NULL,
checkup_payment_id INT NOT NULL,
prescription_payment_id INT NOT NULL,
public_card_no VARCHAR(20) NOT NULL,
total_payment FLOAT NOT NULL,
payment_date DATE NOT NULL,
status VARCHAR(10) NOT NULL,
recorded_by INT NOT NULL,
PRIMARY KEY (transaction_payment_id),
FOREIGN KEY (checkup_payment_id) REFERENCES CHECKUP_PAYMENT(checkup_payment_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (prescription_payment_id) REFERENCES PRESCRIPTION_PAYMENT(prescription_payment_id)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (public_card_no) REFERENCES PUBLIC_PATIENT(id_card_no)
ON UPDATE CASCADE ON DELETE CASCADE,
FOREIGN KEY (recorded_by) REFERENCES CASHIER(cashier_id)

ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE ID_TRACKER(
tracker_id INTEGER PRIMARY KEY,
dummy VARCHAR(20) NOT NULL
);

INSERT INTO service (id_service, service_name) VALUES (1, 'Policlinic');
INSERT INTO service (id_service, service_name) VALUES (2, 'Pharmacy');
INSERT INTO policlinic (policlinic_id, policlinic_name, service_id) VALUES (1, 'General', '1');
INSERT INTO policlinic (policlinic_id, policlinic_name, service_id) VALUES (2, 'Dentist', '1');
INSERT INTO policlinic (policlinic_id, policlinic_name, service_id) VALUES (3, 'Counseling', '1');
INSERT INTO policlinic (policlinic_id, policlinic_name, service_id) VALUES (4, 'Radiologist', '1');

INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (1, 'Monday', 'Morning', '[1999-04-04 09:00, 1999-04-04 10:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (2, 'Monday', 'Morning', '[1999-04-04 10:00, 1999-04-04 11:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (3, 'Monday', 'Morning', '[1999-04-04 11:00, 1999-04-04 12:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (4, 'Monday', 'Afternoon', '[1999-04-04 13:00, 1999-04-04 14:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (5, 'Monday', 'Afternoon', '[1999-04-04 14:00, 1999-04-04 15:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (6, 'Monday', 'Afternoon', '[1999-04-04 15:00, 1999-04-04 16:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (7, 'Monday', 'Afternoon', '[1999-04-04 16:00, 1999-04-04 17:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (8, 'Monday', 'Afternoon', '[1999-04-04 17:00, 1999-04-04 18:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (9, 'Monday', 'Evening', '[1999-04-04 19:00, 1999-04-04 20:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (10, 'Monday', 'Evening', '[1999-04-04 20:00, 1999-04-04 21:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (11, 'Monday', 'Evening', '[1999-04-04 21:00, 1999-04-04 22:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (12, 'Tuesday', 'Morning', '[1999-04-04 09:00, 1999-04-04 10:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (13, 'Tuesday', 'Morning', '[1999-04-04 10:00, 1999-04-04 11:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (14, 'Tuesday', 'Morning', '[1999-04-04 11:00, 1999-04-04 12:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (15, 'Tuesday', 'Afternoon', '[1999-04-04 13:00, 1999-04-04 14:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (16, 'Tuesday', 'Afternoon', '[1999-04-04 14:00, 1999-04-04 15:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (17, 'Tuesday', 'Afternoon', '[1999-04-04 15:00, 1999-04-04 16:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (18, 'Tuesday', 'Afternoon', '[1999-04-04 16:00, 1999-04-04 17:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (19, 'Tuesday', 'Afternoon', '[1999-04-04 17:00, 1999-04-04 18:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (20, 'Tuesday', 'Evening', '[1999-04-04 19:00, 1999-04-04 20:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (21, 'Tuesday', 'Evening', '[1999-04-04 20:00, 1999-04-04 21:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (22, 'Tuesday', 'Evening', '[1999-04-04 21:00, 1999-04-04 22:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (23, 'Wednesday', 'Morning', '[1999-04-04 09:00, 1999-04-04 10:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (24, 'Wednesday', 'Morning', '[1999-04-04 10:00, 1999-04-04 11:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (25, 'Wednesday', 'Morning', '[1999-04-04 11:00, 1999-04-04 12:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (26, 'Wednesday', 'Afternoon', '[1999-04-04 13:00, 1999-04-04 14:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (27, 'Wednesday', 'Afternoon', '[1999-04-04 14:00, 1999-04-04 15:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (28, 'Wednesday', 'Afternoon', '[1999-04-04 15:00, 1999-04-04 16:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (29, 'Wednesday', 'Afternoon', '[1999-04-04 16:00, 1999-04-04 17:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (30, 'Wednesday', 'Afternoon', '[1999-04-04 17:00, 1999-04-04 18:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (31, 'Wednesday', 'Evening', '[1999-04-04 19:00, 1999-04-04 20:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (32, 'Wednesday', 'Evening', '[1999-04-04 20:00, 1999-04-04 21:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (33, 'Wednesday', 'Evening', '[1999-04-04 21:00, 1999-04-04 22:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (34, 'Thursday', 'Morning', '[1999-04-04 09:00, 1999-04-04 10:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (35, 'Thursday', 'Morning', '[1999-04-04 10:00, 1999-04-04 11:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (36, 'Thursday', 'Morning', '[1999-04-04 11:00, 1999-04-04 12:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (37, 'Thursday', 'Afternoon', '[1999-04-04 13:00, 1999-04-04 14:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (38, 'Thursday', 'Afternoon', '[1999-04-04 14:00, 1999-04-04 15:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (39, 'Thursday', 'Afternoon', '[1999-04-04 15:00, 1999-04-04 16:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (40, 'Thursday', 'Afternoon', '[1999-04-04 16:00, 1999-04-04 17:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (41, 'Thursday', 'Afternoon', '[1999-04-04 17:00, 1999-04-04 18:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (42, 'Thursday', 'Evening', '[1999-04-04 19:00, 1999-04-04 20:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (43, 'Thursday', 'Evening', '[1999-04-04 20:00, 1999-04-04 21:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (44, 'Thursday', 'Evening', '[1999-04-04 21:00, 1999-04-04 22:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (45, 'Friday', 'Morning', '[1999-04-04 09:00, 1999-04-04 10:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (46, 'Friday', 'Morning', '[1999-04-04 10:00, 1999-04-04 11:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (47, 'Friday', 'Afternoon', '[1999-04-04 13:00, 1999-04-04 14:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (48, 'Friday', 'Afternoon', '[1999-04-04 14:00, 1999-04-04 15:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (49, 'Friday', 'Afternoon', '[1999-04-04 15:00, 1999-04-04 16:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (50, 'Friday', 'Afternoon', '[1999-04-04 16:00, 1999-04-04 17:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (51, 'Saturday', 'Morning', '[1999-04-04 09:00, 1999-04-04 10:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (52, 'Saturday', 'Morning', '[1999-04-04 10:00, 1999-04-04 11:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (53, 'Saturday', 'Afternoon', '[1999-04-04 13:00, 1999-04-04 14:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (54, 'Saturday', 'Afternoon', '[1999-04-04 14:00, 1999-04-04 15:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (55, 'Saturday', 'Afternoon', '[1999-04-04 15:00, 1999-04-04 16:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (56, 'Saturday', 'Afternoon', '[1999-04-04 16:00, 1999-04-04 17:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (57, 'Sunday', 'Morning', '[1999-04-04 09:00, 1999-04-04 10:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (58, 'Sunday', 'Morning', '[1999-04-04 10:00, 1999-04-04 11:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (59, 'Sunday', 'Afternoon', '[1999-04-04 13:00, 1999-04-04 14:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (60, 'Sunday', 'Afternoon', '[1999-04-04 14:00, 1999-04-04 15:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (61, 'Sunday', 'Afternoon', '[1999-04-04 15:00, 1999-04-04 16:00]');
INSERT INTO shift (shift_id, day, shift_type, time_interval) VALUES (62, 'Sunday', 'Afternoon', '[1999-04-04 16:00, 1999-04-04 17:00]');

INSERT INTO room (room_id, room_name, policlinic_id) VALUES (1, 'G1101', 1);
INSERT INTO room (room_id, room_name, policlinic_id) VALUES (2, 'G1102', 1);
INSERT INTO room (room_id, room_name, policlinic_id) VALUES (3, 'G1103', 1);
INSERT INTO room (room_id, room_name, policlinic_id) VALUES (4, 'G1104', 1);
INSERT INTO room (room_id, room_name, policlinic_id) VALUES (5, 'D1201', 2);
INSERT INTO room (room_id, room_name, policlinic_id) VALUES (6, 'D1202', 2);
INSERT INTO room (room_id, room_name, policlinic_id) VALUES (7, 'C1203', 3);
INSERT INTO room (room_id, room_name, policlinic_id) VALUES (8, 'C1204', 3);
INSERT INTO room (room_id, room_name, policlinic_id) VALUES (9, 'R1304', 4);

INSERT INTO treatment(treatment_id, treatment_name, price, policlinic_id) VALUES ('1', 'Ask for forgiveness', '0', '1');
